import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { toggleShowAll } from '../../logic/actions';
import './styles.css';

export const VisibilityFilter = ({ showAll, onToggleShowAll }) => {
  return (
    <div className="visibilityFilte-wrap">
      <button className="visibilityFilter-button" onClick={() => { onToggleShowAll() } }>
        {!showAll ? 'show all tasks' : 'show only uncompleted tasks' }
      </button>
    </div>
  );
};



VisibilityFilter.propTypes = {
  onToggleShowAll: PropTypes.func.isRequired,
  showAll: PropTypes.bool
};

const mapStateToProps = state => {
  return { showAll: state.todos.showAll };
};


const mapDispatchToProps = dispatch => ({
  onToggleShowAll: visibility => dispatch(toggleShowAll(visibility)),
});

export default connect(mapStateToProps, mapDispatchToProps)(VisibilityFilter);
