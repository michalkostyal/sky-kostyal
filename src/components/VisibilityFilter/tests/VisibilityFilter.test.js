import React from 'react';
import { shallow } from 'enzyme';
import { VisibilityFilter } from '../index';

const defaultProps = {
  showAll: true,
  onToggleShowAll: f => f

};

describe('VisibilityFilter', () => {

  it('renders without crashing', () => {
    shallow(<VisibilityFilter {...defaultProps} />);
  });

  it('should render visibility filter button', () => {
    const renderedItem = shallow(<VisibilityFilter {...defaultProps} />);
    expect(renderedItem.find('.visibilityFilter-button')).toHaveLength(1);
  });

  it('should call onToggleShowAll when remove button clicked', () => {
    const onToggleShowAllMock = jest.fn();

    const renderedItem = shallow(
      <VisibilityFilter {...defaultProps} onToggleShowAll={onToggleShowAllMock}  />
    );
    renderedItem.find('.visibilityFilter-button').simulate('click');
    expect(onToggleShowAllMock.mock.calls.length).toBe(1);
  });


  it('should have text "show all tasks" when visibility filter is on', () => {
    const showAll = false;
    const renderedItem = shallow(
      <VisibilityFilter {...defaultProps} showAll={showAll}  />
    );
    expect(renderedItem.find('.visibilityFilter-button').text()).toEqual('show all tasks');
  });
});