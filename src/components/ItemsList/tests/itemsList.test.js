import React from 'react';
import { shallow, mount } from 'enzyme';
import { ItemsList } from '../index';

const defaultProps = {
  items: [],
  onRemove: f => f,
  onToggleDone: f => f,
  showAll: true
};

describe('ItemsList', () => {
  it('renders without crashing', () => {
    shallow(<ItemsList {...defaultProps} />);
  });

  it('should display warning message if no items', () => {
    const renderedItem = shallow(<ItemsList {...defaultProps} items={[]} />);
    expect(renderedItem.find('#items-missing')).toHaveLength(1);
  });

  it('should not display warning message if items are present', () => {
    const items = [{ id: 1, content: 'Test 1' }];
    const renderedItem = shallow(<ItemsList {...defaultProps} items={items} />);
    expect(renderedItem.find('#items-missing')).toHaveLength(0);
  });

  it('should render items as list items', () => {
    const items = [{ id: 1, content: 'Test 1' }, { id: 2, content: 'Test 2' }];
    const renderedItem = shallow(<ItemsList {...defaultProps} items={items} />);
    expect(renderedItem.find('li')).toHaveLength(2);
  });

  it('should render remove button', () => {
    const items = [{ id: 1, content: 'Test 1' }];
    const renderedItem = shallow(<ItemsList {...defaultProps} items={items} />);
    expect(renderedItem.find('.remove-button')).toHaveLength(1);
  });

  it('should call onRemove when remove button clicked', () => {
    const onRemoveMock = jest.fn();
    const items = [{ id: 1, content: 'Test 1' }];

    const renderedItem = mount(
      <ItemsList {...defaultProps} onRemove={onRemoveMock} items={items} />
    );
    renderedItem.find('.remove-button').simulate('click');
    expect(onRemoveMock.mock.calls.length).toBe(1);
  });

  it('should render item text', () => {
    const items = [{ id: 1, content: 'Test 1' }];
    const renderedItem = shallow(<ItemsList {...defaultProps} items={items} />);
    expect(renderedItem.find('.itemText').text()).toEqual('Test 1');
  });

  it('should call onToggleDone when item cliced', () => {
    const onToggleDoneMock = jest.fn();
    const items = [{ id: 1, content: 'Test 1' }];

    const renderedItem = mount(
      <ItemsList {...defaultProps} onToggleDone={onToggleDoneMock} items={items} />
    );
    renderedItem.find('.itemText').simulate('click');
    expect(onToggleDoneMock.mock.calls.length).toBe(1);
  });

  it('should show only uncompleted tasks when filter when visibility filter on', () => {
    const items = [{ id: 1, content: 'Test 1', completed: false }, { id: 2, content: 'Test 2', completed: true }];
    const showAll = false;

    const renderedItem = mount(
      <ItemsList {...defaultProps}  items={items} showAll={showAll} />
    );

    expect(renderedItem.find('li')).toHaveLength(1);
  });

  it('should by marked as completed (have itemCompleted class) when completed ', () => {
    const items = [{ id: 1, content: 'Test 1', completed: false }, { id: 2, content: 'Test 2', completed: true }];

    const renderedItem = mount(
      <ItemsList {...defaultProps}  items={items}  />
    );

    expect(renderedItem.find('li').first().hasClass('itemCompleted')).toEqual(false);
    expect(renderedItem.find('li').last().hasClass('itemCompleted')).toEqual(true);
  });

});
