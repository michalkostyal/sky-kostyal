import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { removeItem, toggleDone } from '../../logic/actions';
import './styles.css';

export const ItemsList = ({ items, onRemove, onToggleDone, showAll }) => {

  return (
    <div>
      <ul className={'itemsList-ul'}>
        {items.length < 1 && <p id={'items-missing'}>Add some tasks above.</p>}
        {items.map(item =>
          (showAll || !item.completed ) &&
          <li key={item.id} className={item.completed && "itemCompleted" }>
            <span
              className="itemText"
              onClick={() => { onToggleDone(item.id) } }
            >{item.content}</span>

            <span
              className="remove-button"
              onClick={() => { onRemove(item.id) } }
            > x </span>
          </li>
        )}
      </ul>
    </div>
  );
};

ItemsList.propTypes = {
  items: PropTypes.array.isRequired,
  onRemove: PropTypes.func.isRequired,
  onToggleDone: PropTypes.func.isRequired,
  showAll: PropTypes.bool

};

const mapStateToProps = state => {
  return { items: state.todos.items, showAll: state.todos.showAll };
};



const mapDispatchToProps = dispatch => ({
  onRemove: itemId => dispatch(removeItem(itemId)),
  onToggleDone: itemId => dispatch(toggleDone(itemId))
});

export default connect(mapStateToProps, mapDispatchToProps)(ItemsList);
