export const ADD_ITEM = 'qgo/assessment/ADD_ITEM';
export const REMOVE_ITEM = 'qgo/assessment/REMOVE_ITEM';
export const TOGGLE_DONE = 'qgo/assessment/TOGGLE_DONE';
export const TOGGLE_SHOWALL = 'qgo/assessment/TOGGLE_SHOWALL';
