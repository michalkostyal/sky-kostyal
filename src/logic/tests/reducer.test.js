import reducer, { initialState } from '../reducer';
import { addItem } from '../actions';
import {removeItem} from "../actions";
import {toggleDone} from "../actions";
import {toggleShowAll} from "../actions"

describe('reducer', () => {
  it('should return state for unknown action', () => {
    const mockState = { test: 'testItem' };
    const mockAction = { type: 'mystery-meat' };
    const result = reducer(mockState, mockAction);
    expect(result).toEqual(mockState);
  });

  it('should use initial state if state not provided', () => {
    const mockAction = { type: 'mystery-meat' };
    const result = reducer(undefined, mockAction);
    expect(result).toEqual(initialState);
  });

  it('should add new items on ADD_ITEM', () => {
    const mockAction = addItem('Test Content');
    const result = reducer(undefined, mockAction);
    expect(result.items).toHaveLength(4);
    expect(result.items[3].id === 4).toEqual(true);
    expect(result.items[3].content === 'Test Content').toEqual(true);
  });

  it('should remove an item on REMOVE_ITEM', () => {
    const mockAction = removeItem(1);
    const result = reducer(undefined, mockAction);
    expect(result.items).toHaveLength(2);
    expect(result.items[0].id === 2).toEqual(true);
  });

  it('should change state of an item to done on TOGGLE_DONE', () => {
    const mockAction = toggleDone(1);
    const result = reducer(undefined, mockAction);
    expect(result.items[0].completed).toEqual(true);
    expect(result.items[1].completed).toEqual(false);
  });

  it('should toggle view filter on TOGGLE_SHOWALL', () => {
    const mockAction = toggleShowAll();
    const result = reducer(undefined, mockAction);
    expect(result.showAll).toEqual(false);
  });
});
