import { ADD_ITEM, REMOVE_ITEM, TOGGLE_DONE, TOGGLE_SHOWALL } from './constants';

export const addItem = content => {
  return { type: ADD_ITEM, content };
};

export const removeItem = content => {
  return { type: REMOVE_ITEM, content };
};

export const toggleDone = content => {
  return { type: TOGGLE_DONE, content };
};

export const toggleShowAll = () => {
  return { type: TOGGLE_SHOWALL };
};