import { ADD_ITEM, REMOVE_ITEM, TOGGLE_DONE, TOGGLE_SHOWALL } from './constants';

let nextId = 4;

export const initialState = {
  showAll: true,
  items: [
    { id: 1, content: 'Call mum', completed: false },
    { id: 2, content: 'Buy cat food', completed: false },
    { id: 3, content: 'Water the plants', completed: false },
  ],
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case ADD_ITEM:

      const newItem = {
        id: nextId++,
        content: action.content,
      };

      return {
        ...state,
        items: [...state.items, newItem],
      };


    case REMOVE_ITEM:

      const remainingItems = state.items.filter(item => item.id !== action.content);

      return {
        ...state,
        items: remainingItems
      };

    case TOGGLE_DONE:

      const toggledItems = state.items.map(item => {
        if (item.id === action.content) {
           item.completed = !item.completed
        }
        return item
      });

      return {
        ...state,
        items: toggledItems
      };


    case TOGGLE_SHOWALL:

      return {
        ...state,
        showAll: !state.showAll
      };

    default:
      return state;
  }
};

export default reducer;
